Analysis of all dice rolled in roll 20 during a D&D5 campaign

# Webscrapping
*script_webscrap_rolldice.R* contains the webscrapping from the history of all chat messages and rolldice in roll20.
It gather informations about date and player of each message, type of dice (D20, D100...), type of roll (Attacks, damages..) and base value of rolled dice (before adding any bonus)

# R shiny application
The shiny application is available at : https://newraist.shinyapps.io/roll20/